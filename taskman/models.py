from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import BaseUserManager
from django.conf import settings


class MyUserManager(BaseUserManager):
    """
    A custom user manager to deal with emails as unique identifiers for auth
    instead of usernames. The default that's used is "UserManager"
    """
    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The Email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self._create_user(email, password, **extra_fields)




class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=30, null=True)
    first_name=models.CharField(max_length=30, null=True)
    last_name=models.CharField(max_length=30, null=True)

    email = models.EmailField(unique=True, null=True)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    USERNAME_FIELD = 'email'
    objects = MyUserManager()

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email



class Priority (models.Model):
    name = models.CharField(max_length=255)
    value = models.IntegerField()
    def __str__(self):
        return self.name    

# STARTED = 1 #Yellow
# HALFWAY = 2  # Orange
# OVERDUE = 3 # Red
# DONE = 4    # Green
# STATUS_CHOICES = [
#     (STARTED, 'Started'),
#     (HALFWAY, 'Halfway'),
#     (OVERDUE, 'Overdue'),
#     (DONE, 'Done')
#     ]


  
class Category (models.Model):
    name = models.CharField(max_length=255)
    user_id = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        default=None
        )
    def __str__(self):
        return self.name

# class Status (models.Model):
#     percentage = models.IntegerField(default=0)
#     def __str__(self):
#         # return self.name 
#         return "%s %%" %  str(self.percentage)


class Task (models.Model):
    description = models.CharField(max_length=255)
    duedate= models.DateTimeField()
    creation_date = models.DateTimeField(auto_now_add=True)
    is_done = models.BooleanField(default=False) 
    category_id = models.ForeignKey(Category)
    priority_id = models.ForeignKey(Priority)
    percentage = models.IntegerField(default=0)
    color = models.CharField(max_length=30,blank=True )
    responsible = models.CharField(max_length=255,default="Me",)
    user_id = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        )
    #description = models.TextField() 
    def __str__(self):
        # return self.description
        return "%s %s %s%%" % (self.description, self.duedate, self.percentage)


# class Responsible (models.Model):
#     name = models.CharField(max_length=255)
#     task_id = models.ForeignKey(Task, on_delete=models.CASCADE)
#     def __str__(self):
#         return self.name
