from django import forms
from functools import partial
from django.utils import timezone
from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import ugettext, ugettext_lazy as _
from django.forms import Textarea
from django.utils.text import capfirst
from taskman.models import Category

DateInput = partial(forms.DateTimeInput, {'id':'datetimepicker','class': 'flatpickr-input active input','placeholder':'when to get done'})
CharInput = partial(forms.TextInput,{'class': 'input '})
responsibleInput = partial(forms.TextInput,{'class': 'input ','value':'Me'})
percentageInput = partial(forms.TextInput,{'class': 'input ','value': 0})
PasswordInput = partial(forms.TextInput,{'class': 'input ','type':"password"})

PRIORITY_CHOICES = [
    (1, 'High'),
    (2, 'Medium'),
    (3, 'Low'),
]

CATEGORY_CHOICES = [
    (1, 'Default'),
]

class TaskForm(forms.Form):
    description = forms.CharField(max_length=254,widget=CharInput())
    duedate = forms.DateField(label='by date', input_formats=['%Y-%m-%d %H:%M',],
        widget=DateInput())
    priority_id = forms.IntegerField(widget=forms.Select(choices=PRIORITY_CHOICES,attrs={'class': 'input'}))
    category_id = forms.CharField(widget=CharInput())
    # category_id = forms.ModelChoiceField(queryset=Category.objects.all(),widget=forms.Select(attrs={'class': 'input'}))

    responsible = forms.CharField(max_length=254,widget=responsibleInput())
    percentage = forms.CharField(max_length=5,widget=percentageInput())

    def clean(self):
        cleaned_data = super(TaskForm, self).clean()
        description = cleaned_data.get('name')
        duedate = cleaned_data.get('duedate')
        priority_id = cleaned_data.get('priority_id')
        category_id = cleaned_data.get('category_id')
        if not description and not duedate and not priority_id and not category_id:
            raise forms.ValidationError('You have to write something!')

    # def __init__(self, *args, **kwargs):
    #     self.request = kwargs.pop('request')
    #     super(TaskForm, self).__init__(*args, **kwargs)
        # self.fields["category_id"] = forms.ModelChoiceField(queryset=Category.objects.filter(user_id_id=self.request.user.id),widget=forms.Select(attrs={'class': 'input'}))





class LoginForm(forms.Form):
    """
    Base class for authenticating users. Extend this to get a form that accepts
    username/password logins.
    """
    username = forms.CharField(max_length=254,widget=CharInput())
    password = forms.CharField(label=_("Password"), widget=PasswordInput())

    error_messages = {
        'invalid_login': _("Please enter a correct %(username)s and password. "
                           "Note that both fields may be case-sensitive."),
        'inactive': _("This account is inactive."),
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super(LoginForm, self).__init__(*args, **kwargs)

        # Set the label for the "username" field.
        UserModel = get_user_model()
        self.username_field = UserModel._meta.get_field(UserModel.USERNAME_FIELD)
        if self.fields['username'].label is None:
            self.fields['username'].label = capfirst(self.username_field.verbose_name)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            self.user_cache = authenticate(username=username,
                                           password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data



    def confirm_login_allowed(self, user):
            """
            Controls whether the given User may log in. This is a policy setting,
            independent of end-user authentication. This default behavior is to
            allow login by active users, and reject login by inactive users.

            If the given user cannot log in, this method should raise a
            ``forms.ValidationError``.

            If the given user may log in, this method should return None.
            """
            if not user.is_active:
                raise forms.ValidationError(
                    self.error_messages['inactive'],
                    code='inactive',
                )


    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache



class SignUpForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(label=_("Password"),
        widget=PasswordInput())
    password2 = forms.CharField(label=_("Password confirmation"),
        widget=PasswordInput(),
        help_text=_("Enter the same password as above, for verification."))

    email= forms.CharField(label=_("Email"),widget=CharInput())

    class Meta:
        model = get_user_model()
        fields = ("email",)

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        user = super(SignUpForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
            category = Category.objects.create(name = 'Personal',user_id_id = user.id)
            category = Category.objects.create(name = 'Work',user_id_id = user.id)

        return user




class CategoryForm(forms.Form):
    name = forms.CharField(max_length=254,widget=CharInput())
    
    def clean(self):
        cleaned_data = super(CategoryForm, self).clean()
        name = cleaned_data.get('name')
        
        if not name:
            raise forms.ValidationError('You have to write something!')

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(CategoryForm, self).__init__(*args, **kwargs)
