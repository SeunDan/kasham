from django.shortcuts import render
from taskman.models import *
from taskman.forms import *
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic.edit import UpdateView,FormView
from django.contrib.auth import authenticate, get_user_model, login
from django.contrib.auth.decorators import login_required
from django.utils import timezone

# User = get_user_model()

# @login_required(redirect_field_name='login')

def taskcolor(percentage,duedate):
    percentage = int(percentage)
    due = str(duedate)
    time = str(timezone.now())

    if due < time and percentage != 100: 
        color = "red"
        return color
    else:
        grades = [(100, 'green'),  (0, 'yellow')]
        for i in range(len(grades)):
            if percentage >= grades[i][0]:
                return grades[i][1]


def updatecolors(task):
    for i in task:
        # update task colors
        color = taskcolor(i.percentage,i.duedate)
        Task.objects.filter(id=i.id).update(color=color)
    updatedtask = Task.objects.filter(user_id_id=task[0].user_id_id).order_by("is_done","priority_id_id",)
    return updatedtask


def convert(items, id):
    new_dict = items.copy()
    for key, value in items.items():
        if key in id:
            new_key = id[key]
            new_dict[new_key] = items[key] # Copy the value
            del new_dict[key]
    return new_dict


@login_required()
def task_add(request):
    username = request.user
    if request.method == 'POST':
        
        form = TaskForm(request.POST)
        if form.is_valid():
        	task = Task.objects.create(
        		description = form.cleaned_data['description'],
        	    duedate = form.cleaned_data['duedate'],
        	    priority_id_id = form.cleaned_data['priority_id'],
        	    category_id_id = form.cleaned_data['category_id'],
                responsible = form.cleaned_data['responsible'],
        	    user_id_id = request.user.id,
                percentage = form.cleaned_data['percentage'],
                color = taskcolor(form.cleaned_data['percentage'],form.cleaned_data['duedate']),
        	    )
        	
        	return redirect('task_view')
    else:
        form = TaskForm()
        form.fields['category_id'] = forms.ModelChoiceField(queryset=Category.objects.filter(user_id_id=request.user.id),widget=forms.Select(attrs={'class': 'input'}))
    return render(request, 'task_add.html', {'form': form, 'username': username.email,})


@login_required()
def task_view(request):
    username = request.user
    task = Task.objects.filter(user_id_id=username.id).order_by("is_done","priority_id_id",)
    if task:
        updatedtask = updatecolors(task)
    else:
        updatedtask = task
    return render(request, 'task_view.html', {'task': updatedtask, 'username': username.email})

@login_required()
def task_update(request,task_id):
    username = request.user
    if request.method == 'POST':
        
        form = TaskForm(request.POST)
        if form.is_valid():
        	task = Task.objects.filter(id=task_id).update(
        		description = form.cleaned_data['description'],
        	    duedate = form.cleaned_data['duedate'],
        	    priority_id_id = form.cleaned_data['priority_id'],
        	    category_id_id = form.cleaned_data['category_id'],
                responsible = form.cleaned_data['responsible'],
                percentage = form.cleaned_data['percentage'],
                color = taskcolor(form.cleaned_data['percentage'],form.cleaned_data['duedate']),
        	    )
        	
        	return redirect('task_view')
    else:
        task = Task.objects.get(id=task_id)
        data = {'description': task.description, 'duedate': task.duedate, 'priority_id':task.priority_id_id, 'category_id':task.category_id_id,'responsible':task.responsible,'percentage':task.percentage}
        form = TaskForm(initial=data)
        form.fields['category_id'] = forms.ModelChoiceField(queryset=Category.objects.filter(user_id_id=request.user.id),widget=forms.Select(attrs={'class': 'input'}))
        
    return render(request, 'task_add.html', {'form': form, 'username': username.email,})



@login_required()
def task_done(request, task_id):
    task = Task.objects.get(pk=task_id)
    task.is_done = True
    task.save()
    return HttpResponseRedirect(request.POST['next'])

@login_required()
def task_undone(request, task_id):
    task = Task.objects.get(pk=task_id)
    task.is_done = False
    task.save()
    return HttpResponseRedirect(request.POST['next'])

@login_required()
def task_delete(request, task_id):
    task = Task.objects.get(pk=task_id)
    task.delete()
    return HttpResponseRedirect(request.POST['next'])


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('email')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(email=username, password=raw_password)
            login(request, user)
            return redirect('task_view')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})



@login_required()
def category_add(request):
    username = request.user
    if request.method == 'POST':
        
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = Category.objects.create(
                name = form.cleaned_data['name'],
                user_id_id = request.user.id
                )
            
            return redirect('category_add')
    else:
        catlist=Category.objects.filter(user_id_id=request.user.id)
        form = CategoryForm(request=request)
    return render(request, 'category_add.html', {'form': form,'catlist':catlist, 'username': username.email,})

@login_required()
def category_delete(request, category_id):
    category = Category.objects.get(pk=category_id)
    category.delete()
    return HttpResponseRedirect(request.POST['next'])