#Taskman URL Configuration

from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$',views.task_view,name='task_view'),
    url(r'^task_add/$',views.task_add,name='task_add'),
    url(r'^task_update/(?P<task_id>\d+)',views.task_update,name='task_update'),
    url(r'^task_done/(?P<task_id>[\w|\W]+)',views.task_done,name='task_done'),
    url(r'^task_undone/(?P<task_id>[\w|\W]+)',views.task_undone,name='task_undone'),
    url(r'^task_delete/(?P<task_id>[\w|\W]+)',views.task_delete,name='task_delete'),
    url(r'^category_add/$',views.category_add,name='category_add'),
       url(r'^category_delete/(?P<category_id>[\w|\W]+)',views.category_delete,name='category_delete'),
    url(r'^signup/$', views.signup, name='signup'),
]