import os
import sys
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "kasham.settings")

import django
django.setup()

from taskman.models import Priority 

priority = Priority.objects.create(name = 'High',value = 3)
priority = Priority.objects.create(name = 'Medium',value = 2)
priority = Priority.objects.create(name = 'Low',value = 1)